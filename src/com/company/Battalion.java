package com.company;

/**
 * Created by Olesya on 03.05.2017.
 */
public class Battalion extends Company implements Prototype {
    private String equipment;
    private String ammunition;
    private String fightersInfo;

    @Override
    public String toString() {
        return "Battalion{" +
                "equipment='" + equipment + '\'' +
                ", ammunition='" + ammunition + '\'' +
                ", fightersInfo='" + fightersInfo + '\'' +
                '}';
    }

    public Battalion(String fightersInfo, String ammunition, String equipment) {
        super(fightersInfo, ammunition);
        this.fightersInfo = fightersInfo;
        this.ammunition = ammunition;
        this.equipment = equipment;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    @Override
    public String getAmmunition() {
        return ammunition;
    }

    @Override
    public void setAmmunition(String ammunition) {
        this.ammunition = ammunition;
    }

    @Override
    public String getFightersInfo() {
        return fightersInfo;
    }

    @Override
    public void setFightersInfo(String fightersInfo) {
        this.fightersInfo = fightersInfo;
    }

    @Override
    public Battalion doClone() {
        return new Battalion(fightersInfo, ammunition, equipment);
    }
}
