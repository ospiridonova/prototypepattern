package com.company;

/**
 * Created by Olesya on 03.05.2017.
 */
public class Company extends Platoon implements Prototype{

    private String ammunition;
    private String fightersInfo;

    @Override
    public String toString() {
        return "Company{" +
                "ammunition='" + ammunition + '\'' +
                '}';
    }

    public Company(String fightersInfo, String ammunition) {
        super(fightersInfo);
        this.ammunition = ammunition;
    }

    public String getAmmunition() {
        return ammunition;
    }

    public void setAmmunition(String ammunition) {
        this.ammunition = ammunition;
    }

    @Override
    public String getFightersInfo() {
        return fightersInfo;
    }

    @Override
    public void setFightersInfo(String fightersInfo) {
        this.fightersInfo = fightersInfo;
    }

    @Override
    public Company doClone() {
        return new Company(fightersInfo, ammunition);
    }
}
