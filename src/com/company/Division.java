package com.company;

/**
 * Created by Olesya on 03.05.2017.
 */
public class Division extends Regiment implements Prototype {
    private String equipment;
    private String ammunition;
    private String fightersInfo;

    public Division(String fightersInfo, String ammunition, String equipment) {
        super(fightersInfo, ammunition, equipment);
    }

    @Override
    public String getEquipment() {
        return equipment;
    }

    @Override
    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    @Override
    public String getAmmunition() {
        return ammunition;
    }

    @Override
    public void setAmmunition(String ammunition) {
        this.ammunition = ammunition;
    }

    @Override
    public String getFightersInfo() {
        return fightersInfo;
    }

    @Override
    public void setFightersInfo(String fightersInfo) {
        this.fightersInfo = fightersInfo;
    }

    @Override
    public Division doClone() {
        return new Division(fightersInfo, ammunition, equipment);
    }
}
