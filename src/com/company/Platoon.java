package com.company;

/**
 * Created by Olesya on 03.05.2017.
 */
public class Platoon extends Subdivision implements Prototype{

    private String fightersInfo;

    public Platoon(String fightersInfo) {
        super(fightersInfo);
    }

    @Override
    public String getFightersInfo() {
        return fightersInfo;
    }

    @Override
    public void setFightersInfo(String fightersInfo) {
        this.fightersInfo = fightersInfo;
    }

    @Override
    public Platoon doClone() {
        return new Platoon(fightersInfo);
    }
}
