package com.company;

/**
 * Created by Olesya on 03.05.2017.
 */
public interface Prototype {

    public Prototype doClone();

}