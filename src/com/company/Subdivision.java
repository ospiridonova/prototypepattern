package com.company;

/**
 * Created by Olesya on 03.05.2017.
 */
public class Subdivision implements Prototype {
     private String fightersInfo;

    public Subdivision(String fightersInfo) {
        this.fightersInfo = fightersInfo;
    }

    @Override
    public String toString() {
        return "Subdivision{" +
                "fightersInfo='" + fightersInfo + '\'' +
                '}';
    }

    public String getFightersInfo() {
        return fightersInfo;
    }

    public void setFightersInfo(String fightersInfo) {
        this.fightersInfo = fightersInfo;
    }

    @Override
    public Subdivision doClone() {
        return new Subdivision(fightersInfo);
    }
}
